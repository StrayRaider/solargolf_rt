import pygame, sys, os
from solar_lib import image, animation, sound, settings, physical, planet, meteor, rocket

pygame.init()
FPSCLOCK = pygame.time.Clock()

def get_screen_info():
    s_info = pygame.display.Info()
    s_width = s_info.current_w
    s_height = s_info.current_h
    # Ekran Genişliği 16 ya 9 mu anlayalım
    if s_width/s_height == 16/9:
        settings.SCALE_FACTOR = s_width/3840
        settings.SCREEN_WIDTH = s_width
        settings.SCREEN_HEIGHT = s_height
    else:
        x_scale_factor = s_width / 3840
        y_scale_factor = s_height / 2160
        if x_scale_factor >= y_scale_factor:
            settings.SCALE_FACTOR = y_scale_factor
            settings.SCREEN_WIDTH = int(3840*settings.SCALE_FACTOR)
            settings.SCREEN_HEIGHT = s_height
        else:
            settings.SCALE_FACTOR = x_scale_factor
            settings.SCREEN_WIDTH = s_width
            settings.SCREEN_HEIGHT = int(2160*settings.SCALE_FACTOR)

get_screen_info()
settings.SCREEN = pygame.display.set_mode((settings.SCREEN_WIDTH,settings.SCREEN_HEIGHT))

#os.path.join(os.getcwd(),"assets","musics","connect.wav")

rocket_ = rocket.Rocket([2500,1000])
rocket_.set_vector([-1,0])
rocket_2 = rocket.Rocket([1000,1500])
rocket_2.set_vector([1,-1])

bg = image.Image(os.path.join("assets","backgrounds","background_1_1366x768.png"),[0,0],False)


planet_1 = image.Image(os.path.join("assets","planets","planet_1.png"),
                    [1200*settings.SCALE_FACTOR,1200*settings.SCALE_FACTOR])

x = 0

disconnect = sound.Sound(os.path.join(os.getcwd(),
             "assets","musics","disconnect.wav"),"sound")
connect = sound.Sound(os.path.join(os.getcwd(),
             "assets","musics","connect.wav"),"sound")

music_1 = sound.Sound(os.path.join(os.getcwd(), 
             "assets","musics","m_1.wav"),"music")
music_1.play()


#p_1 = physical.Physical("rectangle",[(0,0),(100,100)])
#p_2 = physical.Physical("rectangle",[(120,0),(180,100)])
#p_3 = physical.Physical("circle",[(0,0),100])
#p_1.is_crush(p_2.physic_type,p_2.info_list)
#p_1.is_crush(p_3.physic_type,p_3.info_list)

planet_list = []
meteor_list = []

planet_2 = planet.Planet(os.path.join("assets","planets","planet_1.png"),
           [1500,1000])
planet_list.append(planet_2)

meteor_1 = meteor.Meteor([1000,1500],30)
meteor_list.append(meteor_1)

while True:
    #tuş eventleri
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            rocket.start_animation()
            print("basıldı")
            disconnect.play()
        elif event.type == pygame.MOUSEBUTTONUP:
            pass
        elif event.type == pygame.MOUSEMOTION:
            pass

        elif event.type == pygame.KEYDOWN:
            pass
        elif event.type == pygame.KEYUP:
            pass

    settings.SCREEN.fill((0,0,0))
    x+=1
    #burn.rotate(x)
    #burn.draw()
    #rocket_.rotate(x)
    rocket_.update()
    rocket_2.update()
    rocket_.draw()
    rocket_2.draw()
    for planet in planet_list:
        planet.draw()
    # her bir meteor için
    for meteor in meteor_list:
        #her bir gezegen
        for planet in planet_list:
            #eğer çarpışıuorsa
            if meteor.physic.is_crush(planet.physic):
                meteor.is_crushed = True
        if not meteor.is_crushed:
            meteor.update()
            meteor.draw()
        else:
            meteor.crush_animation.draw()
            if meteor.crush_animation.is_play == False:
                meteor_list.remove(meteor)
    for planet in planet_list:
        if rocket_.physic.is_crush(planet.physic):
            pass
        if rocket_.physic.is_crush(planet.orbit.physic):
            pass
        if rocket_2.physic.is_crush(planet.orbit.physic):
            pass

    #planet_1.draw()

    pygame.display.flip()
    FPSCLOCK.tick(60)
