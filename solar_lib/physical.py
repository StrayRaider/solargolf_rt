import pygame, math
from solar_lib import settings

class Physical():
    def __init__(self,parent,radius = False):
        #circle, rectangle
        self.physic_type = parent.physic_type
        self.parent = parent
        self.is_active = True

    def is_crush(self,physic_obj):
        #physic_obj.parent.center[0]
        if self.is_active:
            distance = 0
            x = False	
            y = False
            if self.physic_type == "circle" and physic_obj.physic_type == "circle":
                distance = math.sqrt(pow(self.parent.center[0] - physic_obj.parent.center[0],2)+pow(self.parent.center[1] - physic_obj.parent.center[1],2))
                if distance <= (self.parent.radius + physic_obj.parent.radius):
                    #print(physic_obj, "çarpar",self.parent)
                    physic_obj.parent.crushed(self.parent)
                    self.parent.crushed(physic_obj.parent)
                    #print(type(self.parent), type(physic_obj.parent))
                    return True
    """
            elif self.physic_type == "rectangle" and other_physic_type == "rectangle":
                for i in range(other_info_list[0][0],other_info_list[1][0]):
                    if self.info_list[0][0] <= i <= self.info_list[1][0]:
                        x = True
                        break
                if x:
                    for i in range(other_info_list[0][1],other_info_list[1][1]):
                        if self.info_list[0][1] <= i <= self.info_list[1][1]:
                            y = True
                            break
                if x and y:
                    print("çarpar")
                    return True

            else:
                if self.physic_type == "circle":
                    dist_line_up = self.dist(*other_info_list[0], *(other_info_list[1][0],other_info_list[0][1]),
                                   *self.info_list[0],self.info_list[1])
                    if dist_line_up:
                        print("çarpar")
                        return True
                    dist_line_left = self.dist(*other_info_list[0],*(other_info_list[0][0],other_info_list[1][1]),
                                   *self.info_list[0],self.info_list[1])
                    if dist_line_left:
                        print("çarpar")
                        return True
                    dist_line_down = self.dist(*other_info_list[1], *(other_info_list[0][0],other_info_list[1][1]),
                                   *self.info_list[0],self.info_list[1])
                    if dist_line_down:
                        print("çarpar")
                        return True
                    dist_line_right = self.dist(*other_info_list[1],*(other_info_list[1][0],other_info_list[0][1]),
                                   *self.info_list[0],self.info_list[1])
                    if dist_line_right:
                        print("çarpar")
                        return True

                else:
                    dist_line_up = self.dist(*self.info_list[0], *(self.info_list[1][0],self.info_list[0][1]),
                                   *other_info_list[0],other_info_list[1])
                    if dist_line_up:
                        print("çarpar")
                        return True
                    dist_line_left = self.dist(*self.info_list[0],*(self.info_list[0][0],self.info_list[1][1]),
                                   *other_info_list[0],other_info_list[1])
                    if dist_line_left:
                        print("çarpar")
                        return True
                    dist_line_down = self.dist(*self.info_list[1], *(self.info_list[0][0],self.info_list[1][1]),
                                   *other_info_list[0],other_info_list[1])
                    if dist_line_down:
                        print("çarpar")
                        return True
                    dist_line_right = self.dist(*self.info_list[1],*(self.info_list[1][0],self.info_list[0][1]),
                                   *other_info_list[0],other_info_list[1])
                    if dist_line_right:
                        print("çarpar")
                        return True

    #noktanın doğru parçasına olan uzaklığı
    def dist(self, x1, y1, x2, y2, x3, y3,radius): # x3,y3 is the point
        px = x2-x1
        py = y2-y1

        norm = px*px + py*py

        u =  ((x3 - x1) * px + (y3 - y1) * py) / float(norm)

        if u > 1:
            u = 1
        elif u < 0:
            u = 0

        x = x1 + u * px
        y = y1 + u * py

        dx = x - x3
        dy = y - y3

        dist = (dx*dx + dy*dy)**.5
        if dist <= radius:
            return True
"""




