import pygame, os
from pygame import gfxdraw
from solar_lib import image, settings, physical, animation

class Meteor():
    def __init__(self, center, radius):
        self.center = [int(center[0]*settings.SCALE_FACTOR),
                       int(center[1]*settings.SCALE_FACTOR)]
        self.radius = int(radius*settings.SCALE_FACTOR)

        self.left_up_pos = [int(self.center[0]-self.radius), int(self.center[1]-self.radius)]
        self.physic_type = "circle"
        self.physic = physical.Physical(self)
        self.color = (255, 255, 255)
        self.vector = [4,-3]
        self.is_crushed = False
        self.explotion_list = [os.path.join("assets","explotions","explo_2","explo_1.png"),
                               os.path.join("assets","explotions","explo_2","explo_2.png"),
                               os.path.join("assets","explotions","explo_2","explo_3.png")]
        self.crush_animation = animation.Animation(self.explotion_list,self.left_up_pos, False, "goback",50)

    def draw(self):
        gfxdraw.aacircle(settings.SCREEN, *self.center, self.radius, self.color)
        gfxdraw.filled_circle(settings.SCREEN, *self.center, self.radius, self.color)
        #pygame.draw.circle(settings.SCREEN, self.color, self.center, self.radius)
        
    def update(self):
        self.center = (int(self.center[0]+self.vector[0]),int(self.center[1]+self.vector[1]))
        self.left_up_pos = [int(self.center[0]-self.radius), int(self.center[1]-self.radius)]
        #update physics
        #self.physic.info_list[0] = self.center

    def crushed(self, crushed_object_class_name):
        self.crush_animation.set_position(self.left_up_pos)
        self.crush_animation.start_stop()
        self.physic.is_active = False
        #print("meteor çarptı")
        

