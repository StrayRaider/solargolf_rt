import pygame
from solar_lib import settings

class Image():
    def __init__(self, path, position, scale=True):
        self.path = path
        self.position = position
        self.scale = scale
        self.load_image()

    def draw(self):
        settings.SCREEN.blit(self.image, self.position)

    def load_image(self):
        if self.scale:
            self.image = pygame.image.load(self.path)
            self.image = pygame.transform.scale(self.image,(self.image.get_width()*settings.SCALE_FACTOR,
                                                            self.image.get_height()*settings.SCALE_FACTOR))
        else:
            self.image = pygame.image.load(self.path)
            #self.image = pygame.transform.scale(self.image,(self.image.get_width(),
            #                                                self.image.get_height()))
        self.loaded_image = self.image
        self.loaded_position = self.position

    def set_position(self,new_position):
        self.position = new_position
        self.loaded_position = new_position

    def rotate(self, angle):
        self.image = pygame.transform.rotate(self.loaded_image,angle)
        self.position = self.image.get_rect(center=(self.loaded_image.get_width()/2+self.loaded_position[0],
                                                    self.loaded_image.get_height()/2+self.loaded_position[1]))
        self.position = [self.position.x,self.position.y]

    def get_width(self):
        return self.image.get_width()

    def get_height(self):
        return self.image.get_height()

