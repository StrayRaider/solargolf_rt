from solar_lib import image, settings, animation, physical
import pygame, os, math, sys, time

class Rocket():
    def __init__(self, position):
        self.position = [int(position[0]*settings.SCALE_FACTOR),int(position[1]*settings.SCALE_FACTOR)]
        self.image = image.Image(os.path.join("assets","rockets","rocket_0.png"),self.position)

        animation_list = [os.path.join("assets","rockets","rocket_0.png"),
                     os.path.join("assets","rockets","rocket_1.png"),
                     os.path.join("assets","rockets","rocket_2.png")]
        self.burn_animation = animation.Animation(animation_list,
                                             [position[0]*settings.SCALE_FACTOR,
                                              position[1]*settings.SCALE_FACTOR],False,"goback")
        self.vector = [5,0.2]

        self.physic_type = "circle"
        self.physic = physical.Physical(self)
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.center=(self.width/2+self.position[0],
                     self.height/2+self.position[1])
        self.radius = self.image.get_height()
        self.in_planet = False
        self.angle = 0

    def draw(self):
        self.image.draw()
        self.burn_animation.draw()
        
    def set_vector(self,vector):
        self.vector = vector

    def update(self):
        if not self.in_planet:
            self.position = [self.position[0]+self.vector[0],self.position[1]+self.vector[1]]
            self.image.set_position(self.position)
            self.burn_animation.set_position(self.position)
            self.center=[self.width/2+self.position[0],
                         self.height/2+self.position[1]]
        if self.in_planet:
            #print("in planet")
            #self.image.set_position(self.position)
            pass
        #print(self.position)

    def rotate(self,angle):
        self.angle == angle
        self.image.rotate(angle)
        self.burn_animation.rotate(angle)

    def start_animation(self):
        self.burn_animation.start_stop()

    def crushed(self, crushed_object):
        #print(str(crushed_object_class_name))
        #<class 'solar_lib.planet.Planet'>
        if str(type(crushed_object)) == "<class' solar_lib.meteor.Meteor'":
            #print("roket meteora çarptı")
            pass
        #print(type(crushed_object_class_name),"burası")
        if str(type(crushed_object)) == "<class 'solar_lib.planet.Planet'>" :
            #print("roket gezegene girdi")
            #self.in_planet = True
            pass
        if str(type(crushed_object)) == "<class 'solar_lib.orbit.Orbit'>":
            #print("roket yörüngeye girdi","içeri alınıyor")
            #get into planet
            if not self.in_planet:
                input_angle = math.atan((self.center[1]-crushed_object.center[1])/(self.center[0]-crushed_object.center[0]))*180.0/math.pi
                #print(abs(self.center[1]-crushed_object.center[1]))
                #print(abs(self.center[0]-crushed_object.center[0]))
                if self.center[0] < crushed_object.center[0]:
                    self.position[0] += self.radius * abs(math.cos(input_angle))
                elif self.center[0] >= crushed_object.center[0]:
                    self.position[0] -= self.radius * abs(math.cos(input_angle))
                if self.center[1] < crushed_object.center[1]:
                    self.position[1] += self.radius * abs(math.cos(input_angle))
                elif self.center[1] >= crushed_object.center[1]:
                    self.position[1] -= self.radius * abs(math.cos(input_angle))
                if input_angle < 0:
                    print("cv")
                else:
                    print("ccv")
                self.rotate(self.angle+input_angle)
                print(input_angle)
            self.in_planet = True


