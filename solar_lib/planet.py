import pygame
from pygame import gfxdraw
from solar_lib import image, settings, physical , orbit, g_field

class Planet():
    def __init__(self, img_path, center):
        self.center = [int(center[0]*settings.SCALE_FACTOR),
                       int(center[1]*settings.SCALE_FACTOR)]
        self.img_path = img_path
        self.image = pygame.image.load(self.img_path)
        self.radius = int(self.image.get_width()*settings.SCALE_FACTOR/2)
        self.orbit = orbit.Orbit(self.center, self.radius*2)
        self.g_field = g_field.G_Field(self.center, self.radius*3)
        self.physic_type = "circle"
        self.left_up_pos = [int(self.center[0]-self.radius), int(self.center[1]-self.radius)]
        self.image = image.Image(self.img_path, [self.left_up_pos[0], self.left_up_pos[1]])
        self.physic = physical.Physical(self)


    def draw(self):
        self.image.draw()
        self.orbit.draw()
        #self.g_field.draw()

    def crushed(self, crushed_object_class_name):
        #print(crushed_object_class_name)
        #print("planet çarptı")
        pass

