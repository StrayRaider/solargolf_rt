from pygame import gfxdraw
from solar_lib import settings, physical

class G_Field():
    def __init__(self,center, radius):
        self.center = center
        self.radius = radius
        self.physic_type = "circle"
        self.physic = physical.Physical(self)

    def draw(self):
        color = (255, 255, 255)
        gfxdraw.aacircle(settings.SCREEN, *self.center, self.radius, color)

    def crushed(self):
        #print("g_field çarptı")
        pass
