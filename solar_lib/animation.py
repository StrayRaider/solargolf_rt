import pygame
from solar_lib import image, settings

class Animation():
    def __init__(self,path_list,position, is_loop, a_type, time_jump=150):
        #a_type = types:"lineer","goback"
        self.position = position
        self.image_list = self.load_images(path_list)
        self.is_loop = is_loop
        self.a_type = a_type
        self.active_image = -1
        self.is_play = False
        self.go_up = True
        self.time_jump = time_jump
        self.last_draw = 0


    def update_active_image(self):
        if self.go_up:
            self.active_image += 1
        else:
            self.active_image -= 1
        if self.active_image == len(self.image_list):
            if self.a_type == "lineer":
                self.active_image = 0
            elif self.a_type == "goback":
                self.active_image -= 2
                self.go_up = False
            else:
                print("animation type wrong setted lineer")
                self.a_type = "lineer"
        elif self.active_image == len(self.image_list)-1 and self.a_type == "lineer" and not self.is_loop:
            self.stop()
        elif self.active_image == 0 and not self.is_loop and not self.go_up:
            self.stop()
        elif self.active_image == -1:
            self.active_image = 1
            self.go_up = True

    def draw(self):
        if self.is_play:
            tick = pygame.time.get_ticks()
            if self.time_jump + self.last_draw <= tick and self.is_play:
                self.last_draw = tick
                self.update_active_image()
                #print(self.active_image)
            if self.active_image == -1:
                self.image_list[0].draw()
            else:
                self.image_list[self.active_image].draw()

    def start(self):
        self.is_play = True
        self.go_up = True
        self.active_image = -1

    def stop(self):
        self.active_image = 0
        self.is_play = False

    def start_stop(self):
        if self.is_play:
            self.stop()
        else:
            self.start()

    def load_images(self,path_list):
        image_list = []
        for path in path_list:
            image_list.append(image.Image(path,self.position))
        return image_list

    def set_position(self,position, center=None):
        self.position = position
        for image in self.image_list:
            image.position = self.position

    def rotate(self,angle):
        for img in self.image_list:
            img.rotate(angle)

        
