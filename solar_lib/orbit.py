from pygame import gfxdraw
from solar_lib import settings, physical

class Orbit():
    def __init__(self,center, radius):
        self.center = center
        self.radius = radius
        self.physic_type = "circle"
        self.physic = physical.Physical(self)

    def draw(self):
        color = (255, 255, 255)
        gfxdraw.aacircle(settings.SCREEN, *self.center, self.radius, color)

    def crushed(self,crushed_object_class_name):
        pass
        #print("orbit çarptı")
        
