import pygame, os

class Sound():
    def __init__(self, path, sound_or_music):
        self.sound_or_music = sound_or_music
        if self.sound_or_music == "sound":
            self.audio = pygame.mixer.Sound(path)
        if self.sound_or_music == "music":
            self.music = pygame.mixer.music.load(path)
    
    def play(self):
        if self.sound_or_music == "music":
            pygame.mixer.music.play(-1)
        if self.sound_or_music == "sound":
            self.audio.play()

    def pause(self):
        if self.sound_or_music == "music":
            pass
        if self.sound_or_music == "sound":
            pygame.mixer.pause()

    def unpause(self):
        if self.sound_or_music == "music":
            pass
        if self.sound_or_music == "sound":
            pygame.mixer.unpause()

    def stop(self):
        if self.sound_or_music == "sound":
            pygame.mixer.stop()

    def set_volume(self, volume):
        #0 - 1.0 arası değer alır
        self.audio.set_volume(volume)
